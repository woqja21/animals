#include <stdint.h>

#include <xen/xen.h>
#include <xen/grant_table.h>

#include "hypercall-x86_64.h"

void * shared_page;
grant_entry_t * grant_table;
/*

static struct {
        ushort free;
        ushort *refs;
} refalloc;


*/

void offer_page()
{
	uint16_t flags;
	
	gnttab_setup_table_t setup_op;
	setup_op.dom = 0; //DOM_SELF
	setup_op.nr_frames = 1; 
	setup_op.frame_list = grant_table;



//	 set_xen_guest_handle();
	HYPERVISOR_grant_table_op(GNTTABOP_setup_table, &setup_op, 1);
	grant_table[0].domid = 1; //DOM_FRIEND
	grant_table[0].frame = (*(int *)shared_page) >> 12;
	flags = GTF_permit_access & GTF_reading & GTF_writing;
	grant_table[0].flags = flags;
}
/*
static int
allocref(void)
{
        int ref;

        ilock(&refalloc);
        ref = refalloc.free;
        if (ref > 0)
                refalloc.free = refalloc.refs[ref];
        iunlock(&refalloc);
        return ref;
}

static void
freeref(int ref)
{
        ilock(&refalloc);
        refalloc.refs[ref] = refalloc.free;
        refalloc.free = ref;
        iunlock(&refalloc);
}

int
xengrant(domid_t domid, ulong frame, int flags)
{
        int ref;
        grant_entry_t *gt;

        if ((ref = allocref()) < 0)
                printf("out of xengrant refs");
        gt = &grant_table[ref];
        gt->frame = frame;
        gt->domid = domid;
        coherence();
        gt->flags = flags;
        return ref;
}

int
xengrantend(int ref)
{
        grant_entry_t *gt;
        int frame;

        gt = &grant_table[ref];
        coherence();
        if (gt->flags&GTF_accept_transfer) {
                if ((gt->flags&GTF_transfer_completed) == 0)
                        printf("xengrantend transfer in progress");
        } else {
                if (gt->flags&(GTF_reading|GTF_writing))
                        printf("xengrantend frame in use");
        }
        coherence();
        frame = gt->frame;
        gt->flags = GTF_invalid;
        freeref(ref);
        return frame;
}



int main()
{

	offer_page();

	printf("hello XEN \n");

	return 0;
}

*/
