/*
 * matrixMultiplexHook.h
 *
 *  Created on: 2016. 1. 13.
 *      Author: chanhee
 */

#ifndef MATRIXMULTIPLEXHOOK_H_
#define MATRIXMULTIPLEXHOOK_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <assert.h>
#include <errno.h>
#include <sched.h>
#include <unistd.h>
#include <getopt.h>
#include <semaphore.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <sys/time.h>
#include <stdint.h> /* for uint64 definition */
#include <time.h> /* for clock_gettime */

#define BILLION 1000000000L

typedef struct CUDA_DEVICE_PROP *cudaDeviceProp;

typedef struct timespec cudaEvent_t;

static cudaDeviceProp chan_cudaDeviceProp;

struct CUDA_DEVICE_PROP{
        char name[256];	//- GPU : GK107
        size_t totalGlobalMem;	//Memory Data - Total Memory	:	1024MB
	size_t sharedMemPerBlock;	//Memory Data - Shared Mem/Block	:48KB
        int regsPerBlock;	//
        int warpSize;		//Core Data - Warp Size : 32
        size_t memPitch;
        int maxThreadsPerBlock;	//Core Data - Thread/Block : 1024
        int maxThreadsDim[3];//Core Data - Block Dim : 1024x1024x64
        int maxGridSize[3];//Core Data   - Grid Size : 2147483647x65535x65535
        size_t totalConstMem;  //Memory Data - Total Constant : 64KB
        int major;
        int minor;
        int clockRate;  	//Core Data - Shader Clock : 875MHz
        size_t textureAlignment;	// - GPU Texture units: 16
        int deviceOverlap;		// 1 or 0
        int multiProcessorCount;	//  Core Data   - MultiProcessors : 1
        int kernelExecTimeoutEnabled;	//	1 or 0
        int integrated;			// 1 or 0
        int canMapHostMemory;	// 1 or 0
        int computeMode;			// ??
        int concurrentKernels;	//1 or 0
        int ECCEnabled;		//1 or 0
        int pciBusID;		//  Bus ID: 1
        int pciDeviceID;		// Device ID: 10DE-FC2
        int tccDriver;		//1 or 0
};

typedef struct {
  int x;
  int y;
  int z;
} dim3;

dim3 gridDim;
dim3 blockDim;
dim3 threadIdx;
dim3 blockIdx;
dim3 block_size;

typedef enum cudaMemcpyKind
{
	cudaMemcpyHostToHost,
	cudaMemcpyHostToDevice,
	cudaMemcpyDeviceToHost,
	cudaMemcpyDeviceToDevice
}cudaMemcpyKind_t;

typedef enum cudaError
{
	cudaSuccess,
	cudaErrorInvalidDevicePointer,
	cudaErrorInitializationError,
	cudaErrorMemoryAllocation,
	cudaErrorInvalidValue,
	cudaErrorInvalidMemcpyDirection,
	cudaErrorLaunchFailure
}cudaError_t;

/*
typedef enum cudaEvent
{
	EventStart,
	EventEnd
}cudaEvent_t;*/

float runningTime = 0;

static void setEnvPBS_JOBID(){
	key_t key;
	key = ftok("/home/chanhee/membership/matrixMultiplexHook/keyfile",1);
	char keyStr[20];

	sprintf(keyStr,"%d",key);
//	itoa(key,keyStr,10);
//	printf("key : %d\n ",key);
//	printf("keyStr : %s\n ",keyStr);

	setenv("PBS_JOBID",keyStr,0);
	//putenv("PBS_JOBID=1004");
}

void init_shmem_segment(){	//Dom U
    key_t                  key;
    int                    shmid;
    char                  *data;
    char *JOBID;
    int jobID;

    setEnvPBS_JOBID();

    JOBID = getenv("PBS_JOBID");
    printf("jobID_STR : %s\n" , JOBID);

    if (!JOBID){	//PBS
    	printf("No PBS environment -> No shared memory!!\n");
    	return;
     }

    jobID = atoi(JOBID);
    printf("jobID : %d\n",jobID);
    if (jobID <= 0){
    	printf("invalid job ID!!\n");
    	return;
    }

    key = jobID;

    //그랜트 테이블 Dom0에서 생성하였으므로 가져오는 것과 매핑만 들어가면 됨.
    shmid = shmget(key, 0, 0);	//이미 생성되어있는 메모리에 접속하므로 0, null인자로 들어감.

    if(shmid == -1){
    	printf("Error : shmid = -1\n");
    }
    else{
    	printf("shmid : %d \n",shmid);
    }

    data          = (char*) shmat(shmid, (void*) 0, 0);
    printf("shmid -> data (gpuInfo) : %s\n", data);

    if (data == (char*) -1){
    	perror("shmat:");
    	return;
    }

    chan_cudaDeviceProp = (cudaDeviceProp) data;

}

int detach_shmem_segment()
{
  return shmdt((void*) chan_cudaDeviceProp);
}

void read_gpu_info(){	//Dom 0
	 FILE *fin;
	 char output[4096];

	 fin = fopen("/home/chanhee/membership/matrixMultiplexHook/gpuInfo.conf", "r");
	  if (!fin){
	    fprintf(stderr, "GPU Info File not exist\n");
	    return;
	  }

	  while (!feof(fin)){
		  fread(output,sizeof(output),1,fin);
		  printf("%s",output);
	  }

	  //
	  //Parsing -> mapping
	  //

	  chan_cudaDeviceProp->clockRate = 1;
	  chan_cudaDeviceProp->major = 1;
	  chan_cudaDeviceProp->minor = 1;

	  printf(" chan_cudaDeviceProp->major : %d\n", chan_cudaDeviceProp->major);
	  printf(" chan_cudaDeviceProp->minor : %d\n", chan_cudaDeviceProp->minor);


	  fclose(fin);

}

void write_shmem_gpu_info(){	//Dom 0
	  key_t                  key;
	    int                    shmid;
	    char                  *data;
	    char *JOBID;
	    int jobID;

	    setEnvPBS_JOBID();

	    JOBID = getenv("PBS_JOBID");
	    printf("jobID_STR : %s\n" , JOBID);

	    if (!JOBID){	//PBS
	    	printf("No PBS environment -> No shared memory!!\n");
	    	return;
	     }

	    jobID = atoi(JOBID);
	    printf("jobID : %d\n",jobID);
	    if (jobID <= 0){
	    	printf("invalid job ID!!\n");
	    	return;
	    }

	    key = jobID;

	    //그랜트 테이블 할당, 매핑 들어가야함.
	    shmid = shmget(key, sizeof(struct CUDA_DEVICE_PROP), IPC_CREAT|0666);	//새로 prop 크기만큼 할당

	    if(shmid == -1){
	    	printf("Error : shmid = -1\n");
	    }
	    else{
	    	printf("shmid : %d \n",shmid);
	    }

	    data          = (char*) shmat(shmid, (void*) 0, 0);
	    printf("shmat data :  %s \n",data);
	    memcpy(data, chan_cudaDeviceProp, sizeof(struct CUDA_DEVICE_PROP)); //gpu load data -> shmem data

	    printf("shmat chan_cudaDeviceProp :  %s \n",chan_cudaDeviceProp);

	    printf("data size :  %d \n",sizeof(data));	//4
	    printf("chan_cudaDeviceProp size :  %d \n",sizeof(chan_cudaDeviceProp));	//4
}

void initKernel(){

	chan_cudaDeviceProp = (cudaDeviceProp *)malloc(sizeof(struct CUDA_DEVICE_PROP));

	printf("sizeof(struct CUDA_DEVICE_PROP) : %d\n",sizeof(struct CUDA_DEVICE_PROP));



	read_gpu_info();

	write_shmem_gpu_info();

	/*
    size_t totalGlobalMem;	//Memory Data - Total Memory	:	1024MB
	size_t sharedMemPerBlock;	//Memory Data - Shared Mem/Block	:48KB
     int regsPerBlock;	//
     int warpSize;		//Core Data - Warp Size : 32
     size_t memPitch;
     int maxThreadsPerBlock;	//Core Data - Thread/Block : 1024
     int maxThreadsDim[3];//Core Data - Block Dim : 1024x1024x64
     int maxGridSize[3];//Core Data   - Grid Size : 2147483647x65535x65535
*/

	gridDim.x = chan_cudaDeviceProp->maxThreadsDim[0];
	gridDim.y = chan_cudaDeviceProp->maxThreadsDim[1];
	gridDim.z = chan_cudaDeviceProp->maxThreadsDim[2];

	blockDim.x = chan_cudaDeviceProp->maxThreadsDim[0];
	blockDim.y = chan_cudaDeviceProp->maxThreadsDim[1];
	blockDim.z = chan_cudaDeviceProp->maxThreadsDim[2];

	block_size.x = chan_cudaDeviceProp->maxThreadsPerBlock;
	block_size.y = chan_cudaDeviceProp->maxThreadsPerBlock;
	/*
	threadIdx.x = 0; threadIdx.y = 0; threadIdx.z = 0;
	blockIdx.x = 0; blockIdx.y = 0; blockIdx.z = 0;
	block_size.x = 0; block_size.y = 0;*/
}

cudaError_t cudaGetDevice(int *device)
{
	initKernel();		//init

	int devID = chan_cudaDeviceProp->pciBusID;
	device = &devID;

	return cudaSuccess;
}
cudaError_t cudaGetDeviceProperties(cudaDeviceProp * prop, int device)
{

	prop = &chan_cudaDeviceProp;		//prop memcpy

	/*

	prop->name = chan_cudaDeviceProp->name;	//- GPU : GK107
	prop -> totalGlobalMem = chan_cudaDeviceProp -> totalGlobalMem;	//Memory Data - Total Memory	:	1024MB
	prop -> sharedMemPerBlock= chan_cudaDeviceProp -> sharedMemPerBlock;	//Memory Data - Shared Mem/Block	:48KB
	prop -> regsPerBlock= chan_cudaDeviceProp -> regsPerBlock;	//
	prop -> warpSize= chan_cudaDeviceProp -> warpSize;		//Core Data - Warp Size : 32
	prop -> memPitch= chan_cudaDeviceProp -> memPitch;
	prop -> maxThreadsPerBlock = chan_cudaDeviceProp -> maxThreadsPerBlock;	//Core Data - Thread/Block : 1024

	for(i=0 ; i<3 ; i++){
		prop ->  maxThreadsDim[i] = chan_cudaDeviceProp ->  maxThreadsDim[i];//Core Data - Block Dim : 1024x1024x64
		prop ->   maxGridSize[i] = chan_cudaDeviceProp ->   maxGridSize[i];//Core Data   - Grid Size : 2147483647x65535x65535
	}
	prop -> totalConstMem = chan_cudaDeviceProp -> totalConstMem;
	prop -> clockRate = chan_cudaDeviceProp -> clockRate;//Core Data - Shader Clock : 875MHz
	prop -> textureAlignment = chan_cudaDeviceProp -> textureAlignment;;	// - GPU Texture units: 16
	prop -> multiProcessorCount = chan_cudaDeviceProp -> multiProcessorCount;
	prop -> pciBusID = chan_cudaDeviceProp -> pciBusID;
	prop -> pciDeviceID = chan_cudaDeviceProp -> pciDeviceID;
*/
	//생략된것 있음.
	int devID = chan_cudaDeviceProp->pciBusID;
	device = &devID;

	return cudaSuccess;
}



cudaEvent_t start, end;


cudaError_t cudaEventCreate(cudaEvent_t* _event)
{
	int eventNum = 0;

	if(eventNum % 2 == 0){
		_event = &start;	//_event = EventStart
	}
	else{
		_event = &end;		//_event = EventEnd
	}
	eventNum++;

	return cudaSuccess;
}
cudaError_t cudaEventElapsedTime(float* ms, cudaEvent_t start, cudaEvent_t end)
{

	uint64_t diff;

	 diff = BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec;
	 printf("elapsed time = %llu nanoseconds\n", (long long unsigned int) diff);

	 ms = &diff;

	return cudaSuccess;
}
cudaError_t cudaEventRecord(cudaEvent_t _event, int id_num)	//xen minios time.h ....
{

	if(id_num == 0){
		/* measure monotonic time */
		clock_gettime(CLOCK_MONOTONIC, &_event); /* mark start time */	//아마 될걸..??
	}
	else if(id_num == 1){
		 /* measure process CPU time */	//sleep 은 프로세스 CPU time으로 치지 않음!
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &_event);
	}
	else if(id_num == 2){
		 /* measure thread CPU time */
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &_event);
	}
	//printf("monotonic_clock " + monotonic_clock());

	return cudaSuccess;
}
cudaError_t cudaEventSynchronize(cudaEvent_t _event)
{
	return cudaSuccess;
}
cudaError_t cudaEventDestroy(cudaEvent_t _event)
{
	return cudaSuccess;
}

cudaError_t cudaFree(void * devPtr){
	return cudaSuccess;
}

cudaError_t cudaMalloc(void** device, size_t size){
	printf("cudaMalloc!!!! size : %d\n",size);

	//allocated device memory포인터이나 디바이스 메모리가 존재하지 않으므로..음ㄴㅇ..
	//store device name
	//size
	return cudaSuccess;
}

cudaError_t cudaMemcpy(void* dst, void* src, size_t count, cudaMemcpyKind_t kind){
	//mapping 의 문제임 ㅠㅠ 뭐 device memoryr가 없는데 어떻게 해야할지.. 그냥 임의로 범위 정해서 해야하나..?
	if(kind == cudaMemcpyHostToHost){}
	else if(kind == cudaMemcpyHostToDevice){}
	else if(kind == cudaMemcpyDeviceToHost){}
	else if(kind == cudaMemcpyDeviceToDevice){}

	return cudaSuccess;
}




#endif /* MATRIXMULTIPLEXHOOK_H_ */
